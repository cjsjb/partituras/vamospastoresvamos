\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		e2:m e2:m e2:m e2:m

		% coro
		e2:m e2:m a2:m e2:m
		e2:m e2:m c2: b2:7
		a2:m a2:m e4:m b4:7 e2:m
		e2:m

		% estrofa
		e2:m e2:m a2:m e2:m
		e2:m e2:m a2:m e2:m
		e2:7 a2:m a2:m e2:m
		e2:m e2:m c2 b2:7
		b2:7

		% coro
		e2:m e2:m a2:m e2:m
		e2:m e2:m c2: b2:7
		a2:m a2:m e4:m b4:7 e2:m
		e2:m
		e2:m
	}
