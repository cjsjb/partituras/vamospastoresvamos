\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble_8"
		\key e \minor

		R2*4  |
%% 5
		b 8 a 16 g fis 8 g  |
		fis 8 e 4.  |
		d 8 c d c  |
		b, 2  |
		b, 8 cis 16 dis e 8 fis  |
%% 10
		g 8 e 4.  |
		e 8 dis 16 e g 8 fis  |
		b, 2  |
		e 8 fis 16 g a 8 b  |
		c' 8 a 4 fis 8  |
%% 15
		g 8 e fis dis  |
		e 2 ~  |
		e 4 r  |
		e 8 e 16 fis g 8 a  |
		b 8 b 4.  |
%% 20
		fis 8 fis 16 a g 8 fis  |
		e 2  |
		e 8 e 16 fis g 8 a  |
		b 8 b 4.  |
		fis 8 fis 16 a g 8 fis  |
%% 25
		e 2  |
		b 8 c' 16 d' c' 8 b  |
		c' 8 a 4.  |
		a 8 a 16 c' b 8 a  |
		g 2  |
%% 30
		g 8 g 16 a g 8 fis  |
		e 8 ( fis ) g 4  |
		e 8 fis 16 g fis 8 e  |
		b 4 ( a  |
		g 4 fis 8 ) r  |
%% 35
		b 8 a 16 g fis 8 g  |
		fis 8 e 4.  |
		d 8 c d c  |
		b, 2  |
		b, 8 cis 16 dis e 8 fis  |
%% 40
		g 8 e 4.  |
		e 8 dis 16 e g 8 fis  |
		b, 2  |
		e 8 fis 16 g a 8 b  |
		c' 8 a 4 fis 8  |
%% 45
		g 8 e fis dis  |
		e 2 ~  |
		e 2  |
		R2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Va -- mos, pas -- to -- res, va -- mos,
		va -- mos a Be -- lén,
		a ver en e -- se ni -- ño la glo -- ria del E -- dén,
		a ver en e -- se ni -- ño la glo -- ria del E -- dén. __

		E -- se pre -- cio -- so ni -- ño,
		yo me mue -- ro por él,
		sus o -- ji -- tos "me en" -- can -- tan,
		su bo -- qui -- ta tam -- bién.

		El pa -- dre "le a" -- ca -- ri -- cia,
		la ma -- dre mi -- "ra en" él,
		y los dos ex -- ta -- sia -- dos
		con -- tem -- plan a -- quel ser. __

		Va -- mos, pas -- to -- res, va -- mos,
		va -- mos a Be -- lén,
		a ver en e -- se ni -- ño la glo -- ria del E -- dén,
		a ver en e -- se ni -- ño la glo -- ria del E -- dén. __
	}
>>
