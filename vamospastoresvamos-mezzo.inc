\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key e \minor

		R2*4  |
%% 5
		b 8 b 16 b b 8 b  |
		e' 8 e' 4.  |
		d' 8 c' d' c'  |
		b 2  |
		b 8 b 16 b b 8 b  |
%% 10
		e' 8 e' 4.  |
		c' 8 c' 16 c' e' 8 dis'  |
		b 2  |
		c' 8 c' 16 c' c' 8 c'  |
		e' 8 e' 4 b 8  |
%% 15
		c' 8 c' e' dis'  |
		b 2 ~  |
		b 4 r  |
		b 8 b 16 b b 8 b  |
		e' 8 e' 4.  |
%% 20
		c' 8 c' 16 c' e' 8 dis'  |
		b 2  |
		b 8 b 16 b b 8 b  |
		e' 8 e' 4.  |
		c' 8 c' 16 c' e' 8 dis'  |
%% 25
		b 2  |
		e' 8 e' 16 e' e' 8 e'  |
		a' 8 a' 4.  |
		fis' 8 g' 16 a' g' 8 fis'  |
		e' 2  |
%% 30
		g' 8 g' 16 a' g' 8 fis'  |
		e' 8 ( fis' ) g' 4  |
		e' 8 fis' 16 g' fis' 8 e'  |
		fis' 4 ( e'  |
		dis' 4. ) r8  |
%% 35
		b 8 b 16 b b 8 b  |
		e' 8 e' 4.  |
		d' 8 c' d' c'  |
		b 2  |
		b 8 b 16 b b 8 b  |
%% 40
		e' 8 e' 4.  |
		c' 8 c' 16 c' e' 8 dis'  |
		b 2  |
		c' 8 c' 16 c' c' 8 c'  |
		e' 8 e' 4 b 8  |
%% 45
		c' 8 c' e' dis'  |
		b 2 ~  |
		b 2  |
		R2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		Va -- mos, pas -- to -- res, va -- mos,
		va -- mos a Be -- lén,
		a ver en e -- se ni -- ño la glo -- ria del E -- dén,
		a ver en e -- se ni -- ño la glo -- ria del E -- dén. __

		E -- se pre -- cio -- so ni -- ño,
		yo me mue -- ro por él,
		sus o -- ji -- tos "me en" -- can -- tan,
		su bo -- qui -- ta tam -- bién.

		El pa -- dre "le a" -- ca -- ri -- cia,
		la ma -- dre mi -- "ra en" él,
		y los dos ex -- ta -- sia -- dos
		con -- tem -- plan a -- quel ser. __

		Va -- mos, pas -- to -- res, va -- mos,
		va -- mos a Be -- lén,
		a ver en e -- se ni -- ño la glo -- ria del E -- dén,
		a ver en e -- se ni -- ño la glo -- ria del E -- dén. __
	}
>>
